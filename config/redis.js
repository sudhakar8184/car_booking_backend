var redis = require('async-redis');
let redisurl =  process.env.redisurl || ''
if(redisurl){
var client =  redis.createClient(redisurl,{
    auth_pass: process.env.rediskey,                                                                                                                                                           
});
}else{
    var client = redis.createClient();
}

client.on('connect', function() {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});
module.exports = client