require('./../../app/models/user')
require('./../../app/models/driver')
require('./../../app/models/rides')
const ratelimter = require('./../middleware/ratelimter')
module.exports = function (app) {
    app.get('/',(req, res) => {
        res.json({ success: true, meassage: "working good" })
    })
    app.use('/api/v1',[ratelimter(),require('./v1')])
}