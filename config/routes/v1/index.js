const mongoose = require('mongoose')
const router = require('express').Router();
const user = require('../../../app/controllers/signup')
const driver = require('../../../app/controllers/driver')
const is_auth = require('./../../middleware/isauth')
router.post('/signup', user.signup)
router.post('/login', user.login)
router.post('/updatedrivers', driver.updateDrivers)
router.get('/getdrivers', driver.getDrivers)
 router.get('/getUserDetails',[is_auth,user.getUserDetail])
 router.post('/savebooking',driver.saveBooking)
 router.get('/mybookings',[is_auth,driver.getMyBookingList])
module.exports = router
