const redisHelper=require('../../app/services/redis')

module.exports=(params={})=>{

    return async(req,res,next)=>{
        if(!redisHelper.connection) next();
        const clientIP = ((req.headers['x-forwarded-for'] || '').split(',').pop() || req.connection.remoteAddress);
        let key = `${(clientIP == '::1' || clientIP == '::ffff:127.0.0.1') ? '127.0.0.1' : clientIP}`;
        let defaults={
            time:60, //in sec
            allowedLimit:100,
            key:key,
            message:"please try after sometime"
        };
        try{
            let options = Object.assign({}, defaults, params, req.rate_limit || {});
            options.key = 'Rate_Limit_' + options.key;
            const attempts = await redisHelper.operations(options.key).get();
            if ((attempts+4) >= options.allowedLimit) {
                let err = new Error();
                err.code = 'HTTP_TOO_MANY_REQUESTS';
                err.message = options.message;
                return res.status(429).json(err);
            }else{
                const attempts1 = await redisHelper.operations(options.key).increase();
                if (attempts1 === 1) {
                    await redisHelper.operations(options.key).expire(options.time);
                }
            }

            next();


        }catch(err){
         next();
        }
    }
}