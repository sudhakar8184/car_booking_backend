const redisClient=require('./../../config/redis');

module.exports = {
    connection: () => {
        return redisClient.connected;
    },
    operations: (key) => {

        const get = async() => {
            return await redisClient.get(key);
        };
        const set = async (field) => {
            return await redisClient.set(key, field);
        };
        const increase = async () => {
            return await redisClient.incr(key);
        };
        const expire = async (time) => {
            return await redisClient.expire(key, time);
        };
        return { increase, get, set,expire };
    }

};