var mongoose = require('mongoose')
var User = mongoose.model('Users')
var bcrypt = require('bcrypt');
var jwtService = require('./v1/auth');
module.exports = {
  signup: async (req, res) => {
    try {
      let data = JSON.parse(JSON.stringify(req.body));
      let useremail = await User.find({ email: data.email }).count();
      if (!useremail) {
        var user = new User(data);
        user.password = bcrypt.hashSync(data.password, 10);
        let userdata = await user.save();
        if (userdata) return res.json({ success: true });
      } else {
        return res.json({ success: false });
      }
    } catch (error) {
      console.log("error", error);
    }
  },
  login: async (req, res) => {
    try {
      let data = JSON.parse(JSON.stringify(req.body));
      const user = await User.find({ email: data.email });
      if (user.length) {
        console.log(data.password, user);
        if (bcrypt.compareSync(data.password, user[0].password))
          return res.json({
            success: true,
            data: {
              token: jwtService.createToken(user[0]),
              _id: user[0]._id
            }
          });
        else
          throw new Error('please correct the password')
      } else {
        throw new Error('this emial is not registred')
      }
    } catch (error) {
      return res.json({ success: false, data: error });
    }
  },
  getUserDetail: async (req, res) => {
    try {
      const user = await User.findOne({ _id: mongoose.Types.ObjectId(req.user.id) })
      if (user) {
        return res.json({ success: true, data: user })
      }
    }
    catch(error){
      return res.json({ success: false,data:error })
    }
  },

};