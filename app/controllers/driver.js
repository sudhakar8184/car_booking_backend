var mongoose = require('mongoose')
var Driver = mongoose.model('Drivers')
var Ride = mongoose.model('Rides')
module.exports = {
  updateDrivers: async (req, res) => {
    try {
      let promiseAll = []
      let driversData = JSON.parse(JSON.stringify(req.body.data));
      driversData.map((driver) => {
        driver = new Driver(driver);
        promiseAll.push(driver.save())
      })
      promiseAll = await Promise.all(promiseAll)
      if (promiseAll)
        return res.json({ success: true });
    }
    catch (error) {
      return res.json({ success: true, error: error });
    }
  },
  getDrivers: async (req, res) => {
    try {
      let language = JSON.parse(JSON.stringify(req.query.language));
      let Driversdata = language ? await Driver.aggregate([
        { $match: { language: language } },
        { $group: { "_id": "$price", "ids": { $push: "$_id" } } },
        { $sort: { '_id': -1 } }
      ]) : await Driver.aggregate([
        { $group: { "_id": "$price", "ids": { $push: "$_id" } } },
        { $sort: { '_id': -1 } }
      ])
      let driversIds = []
      Driversdata.map((drivers, i) => {
        if (i < (Driversdata.length - 1)) {
          driversIds.push(drivers.ids[0])
        } if (i == (Driversdata.length - 1) && driversIds.length < 3) {
          for (let i = 0; i < (3 - driversIds.length); i++) {
            drivers.ids[i] && driversIds.push(drivers.ids[0])
          }
        }
      })
      Driversdata = await Driver.find({ _id: { $in: driversIds } }).sort({ price: 1 })
      return res.json({ success: true, data: Driversdata });
      // else
      //   return res.json({ success: false });
    }
    catch (error) {
      return res.json({ success: false, data: error });
    }
  },
  saveBooking: async (req, res) => {
    try {

      let rideData = JSON.parse(JSON.stringify(req.body.data));
      if (rideData) {
        const stripe_user_charge = require('stripe')(process.env.stripe);
        const charge = await stripe_user_charge.charges.create({
          amount: Math.round(rideData.ride.price),
          currency: 'INR',
          description: 'Deposit Money to Account',
          source: rideData.stripeToken,
        });
        if (charge.status == 'succeeded') {
          rideData['payment_status'] = true
        } else {
          throw new Error('payment fail')
        }
        let ride = new Ride(rideData)
        await ride.save()
        return res.json({ success: true, data: rideData });
      }
      else
        throw new Error()
    }
    catch (error) {
      return res.json({ success: false, data: error });
    }
  },
  getMyBookingList: async(req,res)=>{
    try {
      const user = await Ride.find({"user.email" : req.user.email }).populate('driver')
      if (user) {
        return res.json({ success: true, data: user })
      }
    }
    catch(error){
      return res.json({ success: false,data:error })
    }
  }
};