const mongoose = require('mongoose')
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const saltRounds = 10;
let Schema = mongoose.Schema
var rideSchema = new Schema({
    ride:{
        from: {
            type: String,
            trim: true,
            default: ''
        },
        to: {
            type: String,
            trim: true,
            default: ''
        },
        distance: {
            type: Number,
            trim: true,
            default: ''
        },
        duration: {
            type: String,
            trim: true,
            default: ''
        },
        price:{
            type: Number,
            trim: true,
            default: ''
        }

    },
    departure: {
        type: String,
        trim: true,
        default: ''
    },
    address: {
        type: String,
        trim: true,
        default: ''
    },
    user: {
        name: {
            type: String,
            trim: true,
            default: ''
        },
        email: {
            type: String,
            trim: true,
            default: ''
        },
        phone: {
            type: String,
            trim: true,
            default: ''
        },
    },
     payment_status:{
        type: Boolean,
        trim: true,
        default: false
     },
    driver: {
        type: Schema.Types.ObjectId,
        ref: 'Drivers'
    }
})

module.exports = mongoose.model('Rides', rideSchema)