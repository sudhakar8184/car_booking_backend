const mongoose = require('mongoose')
let Schema = mongoose.Schema
var driverSchema = new Schema({
    name: {
        type: String,
        trim: true,
        default: ''
    },
    price: {
        type: String,
        trim: true,
        default: ''
    },
    phone: {
        type: Number,
        trim: true,
        default: ''
    },
    language: {
        type: String,
        trim: true,
        default: ''
    }

})

module.exports = mongoose.model('Drivers', driverSchema)